package com.example.newapp;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;


import android.support.v7.widget.RecyclerView ;

public class MyAdapter extends  RecyclerView.Adapter<MyAdapter.ItemViewHolder>{

    private AppCompatActivity context;
    String [] listeanimal;

    public MyAdapter(AppCompatActivity context){
        this.context = context;
        this.listeanimal = AnimalList.getNameArray();
    }

    @NonNull
    @Override
    public MyAdapter.ItemViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,parent,false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ItemViewHolder holder, int position) {
        holder.position = position;
        String nomAnimal = listeanimal[position];
        holder.textNomAnimal.setText(nomAnimal);
        Animal animal = AnimalList.getAnimal(nomAnimal);
        int id = context.getResources().getIdentifier(animal.getImgFile(),"drawable",context.getPackageName());
        holder.imageAnimal.setImageResource(id);
    }

    @Override
    public int getItemCount() {
        return listeanimal.length;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        int position;
        ImageView imageAnimal;
        TextView textNomAnimal;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAnimal = itemView.findViewById(R.id.imageAnimal);
            textNomAnimal = itemView.findViewById(R.id.textNomAnimal);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context,AnimalActivity.class);
            intent.putExtra("animalname",textNomAnimal.getText());

            context.startActivity(intent);
        }
    }


}
