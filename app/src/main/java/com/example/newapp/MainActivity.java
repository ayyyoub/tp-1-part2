package com.example.newapp;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.RecyclerView ;


import android.support.v7.widget.LinearLayoutManager ;
import android.support.v7.widget.DividerItemDecoration ;



public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView   recyclerView = findViewById(R.id.recycler);
        MyAdapter myAdapter = new MyAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(getResources().getDrawable(R.drawable.recyclerview));
        recyclerView.addItemDecoration(divider);
        recyclerView.setAdapter(myAdapter);


    }
}