
package com.example.newapp;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.newapp.R ;
import android.graphics.drawable.Drawable;
import android.widget.Toast;



public class AnimalActivity extends AppCompatActivity {

    Animal animal;

    EditText statut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        String animalname = getIntent().getStringExtra("animalname");
        animal = AnimalList.getAnimal(animalname);

        TextView   nomAnimal = findViewById(R.id.myanimal);
        nomAnimal.setText(animalname);

        ImageView photo = findViewById(R.id.picture);
        int id = getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName());
        photo.setImageResource(id);

        TextView    vieMax = findViewById(R.id.vieMax);
        vieMax.setText(animal.getStrHightestLifespan());

        TextView   gestation = findViewById(R.id.gestation);
        gestation.setText(animal.getStrGestationPeriod());

        TextView  poidsN = findViewById(R.id.poidsN);
        poidsN.setText(animal.getStrBirthWeight());

        TextView poidsA = findViewById(R.id.poidsA);
        poidsA.setText(animal.getStrAdultWeight());

        statut = findViewById(R.id.statut);
        statut.setText(animal.getConservationStatus());



        Button enregistrer = findViewById(R.id.btn_sauv);
        enregistrer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                animal.setConservationStatus(statut.getText().toString());
                Toast.makeText(getApplication(),"nouveau Statut sauvegardé",Toast.LENGTH_LONG).show();
            }
        });

    }
}









